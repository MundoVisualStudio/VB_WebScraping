# _WebScraping: Cantidad de Suscriptores y Tipo de Cambio SBS_

## Contenido
- Solución Visual Basic

## Paquetes Nuget

```sh
- HtmlAgilityPack [1.11.33]
- RestSharp [106.11.7]
```
## Videos Relacionados
| Nombre | Enlace |
| ------ | ------ |
| Visual Basic WebScraping Cantidad de Suscriptores y SBS | ▶️[Enlace](https://www.youtube.com/watch?v=7h5dMYwamqs) |

## Redes Sociales
| Plugin | README |
| ------ | ------ |
| Página de Facebook | 👥 [Enlace](https://www.facebook.com/MundoVisualStudio) |
| Facebook | 👥 [Enlace](https://www.facebook.com/danimedina159) |
| Telegram | 💬 [Enlace](https://t.me/MundoVisualStudio) |



## License
[GoldTech](https://corp-goldtech.com/)
